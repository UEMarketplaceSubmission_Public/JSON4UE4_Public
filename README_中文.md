# 1. JSON4UE4
![LOGO](./README.md.res/01_Images/01_Base/Icon128.png)


You can use this plugin to parsing any JSON File or JSON String with UE4 Blueprints. The parsing speed is very fast.    

**Features:**    
 - Parsing JSON String by UE4 Blueprints
 - Load JSON String From JSON File.

In the following figure, you can see the most of blueprint function nodes of this plugin.


![](./README.md.res/01_Images/01_Base/01_1920x1080.png)

List each node separately below.    

GetJsonObjectFromJsonString:    
![](./README.md.res/01_Images/02_BPNodes/008_BPNode_01.PNG)

LoadJsonStringFromFile:    
![](./README.md.res/01_Images/02_BPNodes/003_BPNode_01.PNG)

ConstructJsonObject:    
![](./README.md.res/01_Images/02_BPNodes/009_BPNode_01.PNG)

Get The Value of JSON Field:    
![](./README.md.res/01_Images/02_BPNodes/010_BPNode_01.PNG)

Convert Relative Path to Full:    
![](./README.md.res/01_Images/02_BPNodes/001_BPNode_01.PNG)

Get Game Config Dir:    
![](./README.md.res/01_Images/02_BPNodes/002_BPNode_01.PNG)

LoadStringFromFile:    
![](./README.md.res/01_Images/02_BPNodes/004_BPNode_01.PNG)

GetStringOfLineBreak:    
![](./README.md.res/01_Images/02_BPNodes/005_BPNode_01.PNG)

GetStringOfTabs_01:    
![](./README.md.res/01_Images/02_BPNodes/006_BPNode_01.PNG)

GetStringOfTabs_02:    
![](./README.md.res/01_Images/02_BPNodes/007_BPNode_01.PNG)
