/*
*  Copyright (c) 2016-2018 YeHaike(841660657@qq.com).
*  All rights reserved.
*  @ Date : 2018/05/03
*
*/

#include "JSON4UE4Demo.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, JSON4UE4Demo, "JSON4UE4Demo" );
